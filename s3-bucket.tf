resource "aws_s3_bucket" "my-bucket-example"{
   bucket = "name-9503-20210303"
   acl = "public-read"
}
resource "aws_s3_bucket_object" "picture-of-cat" {
    bucket =  aws_s3_bucket.my-bucket-example.id
    key = "cat.jpg"
    acl = "public-read"
    source = "./cat.jpg"
}